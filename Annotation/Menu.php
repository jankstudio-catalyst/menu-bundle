<?php

namespace Catalyst\MenuBundle\Annotation;

use Annotation;

/**
 * @Annotation
 */
class Menu
{
    public $selected;
}
