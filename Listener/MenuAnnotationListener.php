<?php

namespace Catalyst\MenuBundle\Listener;

use Doctrine\Common\Annotations\Reader;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Twig\Environment as TwigEnvironment;

use Catalyst\MenuBundle\Annotation\Menu as MenuAnnotation;
use Catalyst\MenuBundle\Menu\Generator as MenuGenerator;

class MenuAnnotationListener
{
    protected $annot_reader;
    protected $menu_gen;
	protected $twig;
    protected $menu_name;

    public function __construct(Reader $annot_reader, MenuGenerator $menu_gen, TwigEnvironment $twig, $menu_name)
    {
        $this->annot_reader = $annot_reader;
        $this->menu_gen = $menu_gen;
	    $this->twig = $twig;
        $this->menu_name = $menu_name;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if (!$event->isMasterRequest())
            return;

        // get controller
        $event_controller = $event->getController();
        if (!is_array($event_controller))
            return;

        list($controller, $method_name) = $event_controller;

        // get reflection class
        try
        {
            $ref_controller = new ReflectionClass($controller);
        }
        catch (ReflectionException $e)
        {
            throw new RuntimeException('Cannot read menu annotation.');
        }

        // get method annotations
        $ref_method = $ref_controller->getMethod($method_name);
        $annotation = $this->annot_reader->getMethodAnnotation($ref_method, MenuAnnotation::class);
        
        // check if we get anything
        if ($annotation == null)
            return;

        $this->selectMenu($annotation->selected);
    }

    protected function selectMenu($selected)
    {
        // get menu
        $menu = $this->menu_gen->getMenu($this->menu_name);

        // set menu selected
        $sel = $menu['index']->get($selected);
        if ($sel != null)
            $sel->setSelected();

        // create twig global variable
        $this->twig->addGlobal($this->menu_name, $menu);
    }
}
